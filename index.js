const http = require('http');
const fs = require('fs');
const url = require('url');


function writeLogs(type, fileName) {
    const time = new Date().getTime();
    const logs = JSON.parse(fs.readFileSync('./Log.json', 'utf8'));
    if (type === 'Add') {
        logs['logs'].push({"massage": `File '${fileName}' was added `, "time": time});
    } else if (type === "Read") {
        logs['logs'].push({"massage": `File '${fileName}' was read`, "time": time});
    }

    fs.writeFileSync("./Log.json", JSON.stringify(logs));
}

module.exports = () => {

    http.createServer(function (request, response) {
        const {pathname, query} = url.parse(request.url, true);
        const path=pathname.split('/');
        console.log(path);

        if (request.method === 'POST' && pathname === '/file') {
            if(query.filename===undefined || query.content===undefined){
                response.writeHead(400, {'Content-type': 'text/html'});
                response.end(`No file name or content`);
            }
            else{
                fs.writeFile(`./file/${query.filename}`, query.content, (err) => {
                    if (err) {
                        response.writeHead(400, {'Content-type': 'text/html'});
                        response.end(`Bed request`);

                    } else {
                            writeLogs("Add", query.filename);
                            response.writeHead(200, {'Content-type': 'text/html'});
                            response.end(`file ${query.filename} was added`);
                    }
                });
            }

        } else {
            if (path[1] === 'file') {
                fs.readFile(`./file/${path[2]}`, (err, content) => {
                    if (err) {
                        response.writeHead(400, {'Content-type': 'text/html'});
                        response.end(`No file with ${path[2]} name`);
                    } else {
                        writeLogs("Read", path[2]);
                        response.writeHead(200, {'Content-type': 'text/html'});
                        response.end(content);
                    }
                });
            } else if (pathname === '/logs') {
                fs.readFile('./Log.json', (err, content) => {
                    if (err) {
                        response.writeHead(400, {'Content-type': 'text/html'});
                        response.end("I don`t know :( ");
                    } else {
                        response.writeHead(200, {'Content-type': 'application/json'});
                        response.end(content);
                    }
                });
            }
            else{
                response.writeHead(400, {'Content-type': 'text/html'});
                response.end("Bad request");
            }
        }
    }).listen(process.env.PORT || 8080);
}

